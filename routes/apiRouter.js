// Import 
var express = require("express");

exports.router = (function() {
  var apiRouter = express.Router();

  apiRouter.use("/v1", require("./v1/apiRouter").router);

  return apiRouter;
})();

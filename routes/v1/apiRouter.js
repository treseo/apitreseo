// Import 
var express = require("express");

exports.router = (function() {
  // Path : /api/v1 
  var apiRouter = express.Router();

  apiRouter.get("/", (req, res) => res.send("Hello World!"));

  return apiRouter;
})();

'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('EnigmaAvancements', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      participationId: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "Participations",
          key: "id"
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      },
      enigmaId: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "Enigmas",
          key: "id"
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      },
      state: {
        type: Sequelize.ENUM("UNAVAILABLE", "AVAILABLE", "VALIDATED", "SKIPED", "FAILED"),
        defaultValue: "UNAVAILABLE"
      },
      resolutionId: {
        type: Sequelize.UUID
      },
      activationDate: {
        type: Sequelize.DATE
      },
      // Autogenerated
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('EnigmaAvancements');
  }
};
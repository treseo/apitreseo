'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Organisation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Organisation --[0..*]-------------[1]-- User
      models.Organisation.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user'
      });

      // Organisation --[0..*]-------------[1]-- TreasureHunt
      models.Organisation.belongsTo(models.TreasureHunt, {
        foreignKey: 'treasureHuntId',
        as: 'treasureHunt'
      });
      
    }
  }
  Organisation.init({
    userId: DataTypes.UUID,
    treasureHuntId: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'Organisation',
  });
  return Organisation;
};
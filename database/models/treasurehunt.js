'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TreasureHunt extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // TreasureHunt --[0..*]-------------[0..*]-- User
      models.TreasureHunt.belongsToMany(models.User, {
        through: models.Organisation,
        foreignKey: 'treasurHuntId',
        otherKey: 'userId',
        as: 'organisators'
      });
      
      // TreasureHunt --[0..*]----------------[1]-- Team
      models.TreasureHunt.belongsToMany(models.Team, {
        foreignKey: 'winnerId',
        as: 'winner'
      });
      
      // TreasureHunt --[1]----------------[0..*]-- User
      models.TreasureHunt.hasMany(models.Participations, {
        as: "participations"
      })

    }
  }
  TreasureHunt.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    startDate: DataTypes.DATE,
    endDate: DataTypes.DATE,
    state: DataTypes.ENUM,
    winnerId: DataTypes.UUID,
    config: DataTypes.JSON
  }, {
    sequelize,
    modelName: 'TreasureHunt',
  });
  return TreasureHunt;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ResolutionTry extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // ResolutionTry --[0..*]-------------[1]-- Resolution
      models.ResolutionTry.belongsTo(models.Resolution, {
        foreignKey: 'resolutionId',
        as: 'resolution'
      });
      
      // ResolutionTry --[0..*]-------------[1]-- EnigmaAvancement
      models.ResolutionTry.hasOne(models.EnigmaAvancement, {
        foreignKey: 'enigmaAvancementId',
        as: 'enigmaAvancement'
      });

    }
  }
  ResolutionTry.init({
    avancementId: DataTypes.UUID,
    triedValue: DataTypes.STRING,
    tryDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'ResolutionTry',
  });
  return ResolutionTry;
};
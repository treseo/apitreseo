'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Team extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // Team --[0..*]-------------[0..*]-- User
      models.Team.belongsToMany(models.User, {
        through: models.Member,
        foreignKey: 'teamId',
        otherKey: 'userId',
        as: "users"
      });

      // Team --[1]----------------[0..*]-- Participation
      models.Team.hasMany(models.Participation, {
        foreignKey: 'teamId',
        as: "participations"
      })
      
    }
  }
  Team.init({
    name: DataTypes.STRING,
    joinCode: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Team',
  });
  return Team;
};
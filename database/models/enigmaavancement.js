'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EnigmaAvancement extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // EnigmaAvancement --[0..*]-------------[1]-- Enigma
      models.EnigmaAvancement.belongsTo(models.Enigma, {
        foreignKey: 'enigmaId',
        as: 'enigma'
      });
      
      // EnigmaAvancement --[0..*]-------------[1]-- Participation
      models.EnigmaAvancement.belongsTo(models.Participation, {
        foreignKey: 'participationId',
        as: 'participation'
      });

      // EnigmaAvancement --[1]-------------[0..*]-- UseClue
      models.EnigmaAvancement.hasMany(models.UseClue, {
        foreignKey: 'clueId',
        as: "clues"
      })

      // EnigmaAvancement --[1]-------------[0..*]-- ResolutionTry
      models.EnigmaAvancement.hasMany(models.ResolutionTry, {
        foreignKey: 'enigmaAvancementId',
        as: "resolutionTries"
      })
    }
  }
  EnigmaAvancement.init({
    participationId: DataTypes.UUID,
    enigmaId: DataTypes.UUID,
    state: DataTypes.ENUM,
    resolutionDate: DataTypes.DATE,
    activationDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'EnigmaAvancement',
  });
  return EnigmaAvancement;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Enigma extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Enigma --[1..*]-------------[1]-- TreasureHunt
      models.Enigma.belongsTo(models.TreasureHunt, {
        foreignKey: 'treasureHuntId',
        as: 'treasureHunt'
      });

      // Enigma --[1]-------------[0..*]-- Clue
      models.Enigma.hasMany(models.Clue, {
        foreignKey: 'enigmaId',
        as: "clues"
      })

      // Enigma --[1]-------------[1..*]-- Resolution
      models.Enigma.hasMany(models.Resolution, {
        foreignKey: 'enigmaId',
        as: "resolutions"
      })
      
      // Enigma --[1]-------------[0..*]-- EnigmaAvancement
      models.Enigma.hasMany(models.EnigmaAvancement, {
        foreignKey: 'enigmaId',
        as: 'avancements'
      });
      
      // Enigma --[1]-------------[0..*]-- Trigger
      models.Enigma.hasMany(models.Trigger, {
        foreignKey: 'enigmaId',
        as: 'triggers'
      });

    }
  }
  Enigma.init({
    treasureHuntId: DataTypes.UUID,
    number: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    points: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Enigma',
  });
  return Enigma;
};
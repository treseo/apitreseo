'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GlobalMessage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // GlobalMessage --[0..*]-------------[0..*]-- Participation
      models.GlobalMessage.belongsToMany(models.Participation, {
        through: models.AcknowledgmentReceipt,
        foreignKey: 'messageId',
        otherKey: 'participationId',
        as: 'participations'
      });

      // GlobalMessage --[0..*]-------------[1]-- TreasureHunt
      models.GlobalMessage.belongsTo(models.TreasureHunt, {
        foreignKey: 'treasureHuntId',
        as: 'treasureHunt'
      });

    }
  }
  GlobalMessage.init({
    treasureHuntId: DataTypes.UUID,
    date: DataTypes.DATE,
    title: DataTypes.STRING,
    message: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'GlobalMessage',
  });
  return GlobalMessage;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AcknowledgmentReceipt extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // AcknowledgmentReceipt --[0..*]-------------[1]-- Participation
      models.AcknowledgmentReceipt.belongsTo(models.Participation, {
        foreignKey: 'participationId',
        as: 'participation',
        onDelete: 'CASCADE'
      });

      // AcknowledgmentReceipt --[0..*]-------------[1]-- GlobalMessage
      models.AcknowledgmentReceipt.belongsTo(models.GlobalMessage, {
        foreignKey: 'messageId',
        as: 'message',
        onDelete: 'CASCADE'
      });
      
    }
  }
  AcknowledgmentReceipt.init({
    messageId: DataTypes.UUID,
    participationId: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'AcknowledgmentReceipt',
  });
  return AcknowledgmentReceipt;
};
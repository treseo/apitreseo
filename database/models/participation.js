'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Participation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Participation --[0..*]-------------[0..*]-- GlobalMessage
      models.Participation.belongsToMany(models.GlobalMessage, {
        through: models.AcknowledgmentReceipt,
        foreignKey: 'participationId',
        otherKey: 'messageId',
        as: 'messages'
      });
   
      // Participation --[0..*]-------------[1]-- Team
      models.Participation.belongsTo(models.Team, {
        foreignKey: 'teamId',
        as: 'team'
      });

      // Participation --[0..*]-------------[1]-- TreasureHunt
      models.Participation.belongsTo(models.TreasureHunt, {
        foreignKey: 'treasureHuntId',
        as: 'treasureHunt'
      });

      // Participation --[1]-------------[0..*]-- EnigmaAvancement
      models.Participation.hasMany(models.EnigmaAvancement, {
        foreignKey: 'participationId',
        as: 'enigmaAvancements'
      });
      
    }
  }
  Participation.init({
    treasureHuntId: DataTypes.UUID,
    teamId: DataTypes.UUID,
    participationCode: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Participation',
  });
  return Participation;
};
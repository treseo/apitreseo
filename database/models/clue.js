'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Clue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Clue --[0..*]-------------[1]-- Enigma
      models.Clue.belongsTo(models.Enigma, {
        foreignKey: 'enigmaId',
        as: 'enigma'
      });

      // Clue --[1]-------------[0..*]-- UseClue
      models.Clue.hasMany(models.UseClue, {
        foreignKey: 'clueId',
        as: 'uses'
      });

    }
  }
  Clue.init({
    enigmaId: DataTypes.UUID,
    clue: DataTypes.STRING,
    malus: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Clue',
  });
  return Clue;
};
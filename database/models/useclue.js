'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UseClue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // UseClue --[0..*]-------------[1]-- Clue
      models.UseClue.belongsTo(models.Clue, {
        foreignKey: 'clueId',
        as: 'clue'
      });
      
      // UseClue --[0..*]-------------[1]-- EnigmaAvancement
      models.UseClue.belongsTo(models.EnigmaAvancement, {
        foreignKey: 'enigmaAvancementId',
        as: 'enigmaAvancement'
      });

    }
  }
  UseClue.init({
    clueId: DataTypes.UUID,
    enigmaAvancementId: DataTypes.UUID,
    state: DataTypes.ENUM,
    malus: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UseClue',
  });
  return UseClue;
};
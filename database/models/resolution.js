'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resolution extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Resolution --[0..*]-------------[1]-- Enigma
      models.Resolution.belongsTo(models.Enigma, {
        foreignKey: 'enigmaId',
        as: 'enigma'
      });

      // Resolution --[1]-------------[0..*]-- ResolutionTry
      models.Resolution.hasMany(models.ResolutionTry, {
        foreignKey: 'resolutionId',
        as: 'resolutionTries'
      });

    }
  }
  Resolution.init({
    enigmaId: DataTypes.UUID,
    value: DataTypes.STRING,
    resolutionType: DataTypes.ENUM
  }, {
    sequelize,
    modelName: 'Resolution',
  });
  return Resolution;
};
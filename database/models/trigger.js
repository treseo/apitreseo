'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => { 
  class Trigger extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // Trigger --[0..*]-------------[1]-- Enigma
      models.Trigger.belongsTo(models.Enigma, {
        foreignKey: 'enigmaId',
        as: 'enigma'
      });

    }
  }
  Trigger.init({
    enigmaId: DataTypes.UUID,
    value: DataTypes.STRING,
    type: DataTypes.ENUM
  }, {
    sequelize,
    modelName: 'Trigger',
  });
  return Trigger;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Member extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Member --[0..*]-------------[1]-- Team
      models.Member.belongsTo(models.Team, {
        foreignKey: 'teamId',
        as: 'team'
      });

      // Member --[0..*]-------------[1]-- User
      models.Member.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user'
      });

    }
  }
  Member.init({
    userId: DataTypes.UUID,
    teamId: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'Member',
  });
  return Member;
};
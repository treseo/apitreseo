'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // User --[0..*]-------------[0..*]-- TreasurHunt
      models.User.belongsToMany(models.TreasurHunt, {
        through: models.Organisation,
        foreignKey: 'userId',
        otherKey: 'treasurHuntId',
        as: 'treasureHunts'
      });
      
      // User --[0..*]-------------[0..*]-- Team
      models.User.belongsToMany(models.Team, {
        through: models.Member,
        foreignKey: 'userId',
        otherKey: 'teamId',
        as: "teams"
      });

    }
  }
  User.init({
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN,
    isEmailConfirm: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
'use strict';
/** @type {import('sequelize-cli').Migration} */
const { v4: uuidv4 } = require('uuid');

module.exports = {
  async up (queryInterface, Sequelize) {

    // -------------------User----------------------
    const userId1 = uuidv4();
    await queryInterface.bulkInsert(
      'Users', 
      [
        {
          id: userId1,
          email: 'user1@test.fr',
          username: 'user1',
          password: 'test',
          isAdmin: false,
          isEmailConfirm: true,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});

    // -------------------TreasureHunt----------------------
    const treasureHuntId1 = uuidv4();
    await queryInterface.bulkInsert(
      'TreasureHunts', 
      [
        {
          id: treasureHuntId1,
          name: 'TreasureHunt 1',
          description: 'description 1',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});

    // -------------------Organisation----------------------
    return await queryInterface.bulkInsert(
      'Organisations', 
      [
        {
          userId: userId1,
          treasureHuntId: treasureHuntId1,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});

  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
    await queryInterface.bulkDelete('TreasureHunts', null, {});
    await queryInterface.bulkDelete('Organisations', null, {});
  }
};

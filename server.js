
// Imports
require("dotenv").config({path: __dirname + "/.env"});
var express = require("express");
var bodyParser = require("body-parser");
const expressJSDocSwagger = require("express-jsdoc-swagger");
const configDoc = require("./docs/api/config.js");

// Instantiate server
var server = express();

// Documentation
expressJSDocSwagger(server)(configDoc);

// Body Parser configuration
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

// Configure routes
server.use("/api/", require("./routes/apiRouter").router);

server.use("/", function(req, res) { 
  res.status(404).json({
    result: "fail",
    error : {
      devMessage : "Request not found",
      userMessage : "Requête inconnue"
    }
  });
});

// Launch server
var app = server.listen(process.env.PORT, function() {
  console.log("API Server started on " + process.env.PORT);
});

function shutDown() {
  app.close(() => {
    console.log("close server");
  });
}

module.exports = app;
module.exports.shutDown = shutDown;

  